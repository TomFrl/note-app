package com.example.noteapp;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {
    private Repository repository;
    private LiveData<List<SingleNote>> allRecords;


    public NoteViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
        allRecords = repository.getAllRecords();

    }

    public void insert (SingleNote note){
        repository.insert(note);
    }
    public void update (SingleNote note){
        repository.update(note);
    }
    public void delete (SingleNote note){
        repository.delete(note);
    }
    public void deleteAllNotes (){
        repository.deleteAllNotes();
    }

    public LiveData<List<SingleNote>> getAllRecords() {
        return allRecords;
    }
}
