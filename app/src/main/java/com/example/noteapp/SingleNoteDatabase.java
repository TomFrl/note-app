package com.example.noteapp;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(version = 1, entities = {SingleNote.class} )
public abstract class SingleNoteDatabase extends RoomDatabase {

    public abstract SingleNoteDao singleNoteDao();

    //Singleton used for instantiating
    private static SingleNoteDatabase instance;

    public static synchronized SingleNoteDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    SingleNoteDatabase.class, "SingleNoteDatabase")
                    .addCallback(roomCallback)
                    .build();


        }
        return instance;

    }
    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private SingleNoteDao singleNoteDao;

        private PopulateDbAsyncTask(SingleNoteDatabase db) {
            singleNoteDao = db.singleNoteDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Async Task adds two records below on database creation.
            singleNoteDao.insert(new SingleNote("Položka 1", "Popisek 1"));
            singleNoteDao.insert(new SingleNote("Položka 2", "Popisek 2"));

            return null;
        }
    }
}
