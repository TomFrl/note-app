package com.example.noteapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SingleNoteAdapter extends RecyclerView.Adapter<SingleNoteAdapter.SingleNoteViewHolder> {

    private List<SingleNote> notes = new ArrayList<>();

    @NonNull
    @Override
    public SingleNoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_note_item, parent, false);
            return new SingleNoteViewHolder(itemView);
    }

    @Override // binds each itemViews with SingleNote Entity records
    public void onBindViewHolder(@NonNull SingleNoteViewHolder holder, int position) {
        SingleNote currentNote = notes.get(position);
        holder.textViewTitle.setText(currentNote.getTitle());
        holder.textViewDescription.setText(currentNote.getDecription());
    }

    @Override //how many items show in recyclerView?
    public int getItemCount() {
        return notes.size();
    }

    //
    public void setNotes (List<SingleNote>notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }
    //gets SingleNote record by int position
    public SingleNote getNoteByPosition (int position){
        return notes.get(position);
    }


    class SingleNoteViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewTitle;
        private TextView textViewDescription;


        public SingleNoteViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription= itemView.findViewById(R.id.text_description);


        }
    }


}
