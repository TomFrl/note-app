package com.example.noteapp;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

@Dao
public interface SingleNoteDao {  //either Abstract class or interface

    @Insert
    void insert (SingleNote note);
    @Update
    void update (SingleNote note);
    @Delete
    void delete (SingleNote note);

    @Query("DELETE FROM notes")
    void deleteAllNotes();

    @Query("SELECT * FROM notes ORDER BY id DESC")
    LiveData<List<SingleNote>>getAllrecords();
}
