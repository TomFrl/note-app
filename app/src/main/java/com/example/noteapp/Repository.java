package com.example.noteapp;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import java.util.List;

public class Repository {

    private SingleNoteDao singleNoteDao;
    private LiveData<List<SingleNote>> allRecords;

    public Repository (Application application){

        SingleNoteDatabase database = SingleNoteDatabase.getInstance(application);
        singleNoteDao = database.singleNoteDao();
        allRecords = singleNoteDao.getAllrecords();

    }

    public void insert (SingleNote note){
        new InsertSingleNoteAsyncTask(singleNoteDao).execute(note);
    }

    public void update (SingleNote note){
        new UpdateSingleNoteAsyncTask(singleNoteDao).execute(note);
    }
    public void delete (SingleNote note){
        new DeleteSingleNoteAsyncTask(singleNoteDao).execute(note);
    }
    public void deleteAllNotes (){
        new DeleteAllNotesAsyncTask(singleNoteDao).execute();
    }

    public LiveData<List<SingleNote>> getAllRecords(){
        return allRecords;
    }


    private static class InsertSingleNoteAsyncTask extends AsyncTask <SingleNote, Void, Void> {
        private SingleNoteDao singleNoteDao;

        private InsertSingleNoteAsyncTask(SingleNoteDao singleNoteDao){
            this.singleNoteDao = singleNoteDao;
        }

        @Override
        protected Void doInBackground(SingleNote... Note) {
            singleNoteDao.insert(Note[0]);
            return null;
        }
    }

    private static class UpdateSingleNoteAsyncTask extends AsyncTask <SingleNote, Void, Void> {
        private SingleNoteDao singleNoteDao;

        private UpdateSingleNoteAsyncTask(SingleNoteDao singleNoteDao){
            this.singleNoteDao = singleNoteDao;
        }

        @Override
        protected Void doInBackground(SingleNote... Note) {
            singleNoteDao.update(Note[0]);
            return null;
        }
    }

    private static class DeleteSingleNoteAsyncTask extends AsyncTask <SingleNote, Void, Void> {
        private SingleNoteDao singleNoteDao;

        private DeleteSingleNoteAsyncTask(SingleNoteDao singleNoteDao){
            this.singleNoteDao = singleNoteDao;
        }

        @Override
        protected Void doInBackground(SingleNote... Note) {
            singleNoteDao.delete(Note[0]);
            return null;
        }
    }

    private static class DeleteAllNotesAsyncTask extends AsyncTask <Void, Void, Void> {
        private SingleNoteDao singleNoteDao;

        private DeleteAllNotesAsyncTask(SingleNoteDao singleNoteDao){
            this.singleNoteDao = singleNoteDao;
        }

        @Override
        protected Void doInBackground(Void...Void) {
            singleNoteDao.deleteAllNotes();
            return null;
        }
    }

}
