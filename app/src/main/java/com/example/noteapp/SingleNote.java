package com.example.noteapp;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "notes")
public class SingleNote {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;
    private String decription;


    public SingleNote(String title, String decription) {
        this.title = title;
        this.decription = decription;
    }

    //id is not passed by constructor, setter therefore needed
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDecription() {
        return decription;
        }
    }



